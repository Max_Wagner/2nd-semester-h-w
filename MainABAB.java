public class MainABAB {
    public static void main(String[] args) {
        Integer x = 10;
        ABAB a = new ABAB("A", x);
        ABAB b = new ABAB("B", x);
        a.start();
        b.start();
    }
}
