import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class print {
    public static void main(String[] args) throws IOException {
        RandomAccessFile f = new RandomAccessFile("a.txt", "r");
        byte[] b = new byte[(int)f.length()];
        f.read(b);
        int [] stat = new int[256];
        int [] st = new int[256];
        Arrays.fill(stat, 0);
        Arrays.fill(st, -1);
        for (int i = 0; i < f.length(); i++) {
            stat[b[i]]++;
            if (st[b[i]] == -1) {
                st[b[i]] = i;
            }
        }
        for (int i = 0; i < 256; i++) {
            if (stat[i] != 0) {
                System.out.println((char) b[st[i]] + " — " + Integer.toString(stat[i]));
            }
        }
    }
}