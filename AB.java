public class AB extends Thread {
    String whoAmI;
    final Object synchoro;

    AB(String whoami, Object sync){
        whoAmI = whoami;
        synchoro = sync;
    }
    void dodo(){
        for (int j = 0; j < 6000; j++) {
            synchronized (synchoro) {
                char x = 'A';
                for (int i = 0; i < 26; i++) {
                    System.out.print(x + " ");
                    x++;
                }
                System.out.println(whoAmI);
            }
        }
    }
    @Override
    public void run(){
        dodo();
    }
}