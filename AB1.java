public class AB1 extends Thread {
    String whoAmI;
    final Object synchoro;

    AB1(String whoami, Object sync){
        whoAmI = whoami;
        synchoro = sync;
    }
    void dodo(){
        for (int i = 0; i< 10; i++) {
            synchronized (synchoro) {
                System.out.print(whoAmI);
            }
        }
    }
    @Override
    public void run(){
        dodo();
    }

}