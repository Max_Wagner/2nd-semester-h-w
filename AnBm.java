public class AnBm extends Thread {
    String whoAmI;
    final Object synchro;
    int n;

    AnBm(String whoami, Object sync, int m){
        whoAmI = whoami;
        synchro = sync;
        n = m;
    }
    void dodo(){
        synchronized (synchro) {
            for (int j = 0; j < n; j++) {
                System.out.print(whoAmI);
                synchro.notifyAll();
                try {
                    synchro.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            synchro.notifyAll();
        }
    }
    @Override
    public void run(){
        dodo();
    }
}
