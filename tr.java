import java.util.Arrays;

public class tr {
    public static String [] l;
    public static void stat (Tree t, String s){
        if (t.left == null) {
            l[(int) t.id - 65] = s;
        }
        else {
            stat(t.left, s+"0");
            stat(t.right, s+"1");
        }
    }
    public static Tree readtree () {
        return new Tree((byte) 69);
    }
    public static void main(String[] args) {
        Tree t = new Tree(new Tree(new Tree(new Tree((byte) 65), new Tree(new Tree((byte) 68), new Tree((byte) 83))),new Tree((byte) 78)), new Tree(new Tree((byte) 76), new Tree((byte) 90)));
        String s = "ADNSZNLS";
        l = new String[26];
        Arrays.fill(l, "");
        stat(t, "");
        String ss = "";
        for (int i = 0; i < s.length(); i++) {
            ss = ss + l[(int) ((byte) s.charAt(i)) - 65];
        }
        System.out.println(ss);
    }
}