public class ABAB extends Thread {
    String whoAmI;
    final Object synchro;

    ABAB(String whoami, Object sync){
        whoAmI = whoami;
        synchro = sync;
    }
    void dodo(){
        synchronized (synchro) {
            for (int j = 0; j < 1000; j++) {
                System.out.print(whoAmI);
                synchro.notifyAll();
                try {
                    synchro.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            synchro.notifyAll();
        }
    }
    @Override
    public void run(){
        dodo();
    }
}
