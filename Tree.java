public class Tree {
    public Tree left;
    public Tree right;
    public byte id;

    public Tree(Tree x, Tree y) {
        left = x;
        right = y;
    }

    public Tree(byte i) {
        left = right = null;
        id = i;
    }
}