import java.net.InetAddress;
 
public class sett {

    public static void main(String argv[]) throws Exception {
        InetAddress host = InetAddress.getLocalHost();
        byte ip[] = host.getAddress();
        System.out.println(host.getHostName());
        for (int i = 0; i < ip.length; i++) {
            if (i > 0) {
                System.out.print(".");
            }
            System.out.print(ip[i] & 0xff);
        }
        System.out.println();
        Process pr = Runtime.getRuntime().exec("getmac /fo csv /nh");
        java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(pr.getInputStream()));
        String line = in.readLine();
        String[] res = line.split(",");
        System.out.println(res[0].replace('"', ' ').trim());
    }
}