import java.util.Scanner;

public class mac {
    public static boolean is_ok (char i) {
        return (i == '0')||(i == '1')||(i == '2')||(i == '3')||(i == '4')||(i == '5')||(i == '6')||(i == '7')||(i == '8')||(i == '9')||(i == 'A')||(i == 'B')||(i == 'C')||(i == 'D')||(i == 'E')||(i == 'F');
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String st = s.nextLine();
        st = st + "-";
        boolean flag = true;
        int sost = 0;
        for (int i = 0; i < st.length(); i++) {
            if (st.charAt(i) != '-') {
                flag = (sost < 2) && (is_ok(st.charAt(i)));
                sost++;
            }
            if (st.charAt(i) == '-') {
                if (sost != 2) {
                    flag = false;
                }
                else {
                    sost = 0;
                }
            }
            if (!flag) {
                break;
            }
        }
        if (flag) {
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }
    }
}