import java.util.Arrays;
import java.util.Scanner;

public class phi {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int j = 0;
        int [] x = new int[n];
        int [] y = new int[n];
        int [] l = new int[n];
        Arrays.fill(l, 1);
        int m = n;
        for (int i = 2; i <= ((int) Math.sqrt((double) n)); i++) {
            if (m == 1)
                break;
            if (m%i == 0) {
                for (int k = 0; k < n; k++) {
                    if (k % i == 0) {
                        l[k] = 0;
                    }
                }
                x[j] = i;
                y[j]++;
                j = j + 1;
                m = m/i;
                while (m%i == 0) {
                    if (m == 1)
                        break;
                    m = m/i;
                    y[j - 1]++;
                }
            }
        }
        int phi = 0;
        for (int i = 0; i < n; i++) {
            if (l[i] != 0) {
                phi++;
            }
        }
        System.out.println("phi(n) = " + Integer.toString(phi));
        System.out.print(Integer.toString(n)+" = ");
        for (int i = 0; i < j; i++) {
            System.out.print(x[i]);
            System.out.print('^');
            System.out.print(y[i]);
            if (i != j - 1)
                System.out.print(" * ");
        }
    }
}
