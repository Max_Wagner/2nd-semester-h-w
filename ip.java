import java.util.Scanner;

public class ip {
    public static int prib (char i, int in) {
        switch (i){
            case '0': return in*10;
            case '1': return in*10 + 1;
            case '2': return in*10 + 2;
            case '3': return in*10 + 3;
            case '4': return in*10 + 4;
            case '5': return in*10 + 5;
            case '6': return in*10 + 6;
            case '7': return in*10 + 7;
            case '8': return in*10 + 8;
            case '9': return in*10 + 9;
            default: return -1;
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String st = s.nextLine();
        st = st +"*";
        int ch = 0;
        boolean flag = true;
        int sost = 0;
        int in = 0;
        for (int i = 0; i < st.length(); i++) {
            if ((st.charAt(i) == '0') && (sost == 0)) {
                flag = st.charAt(i+1) == '.';
                sost = 1;
            }
            if (((st.charAt(i) != '0') && (st.charAt(i) != '.') && (st.charAt(i) != '*') && (sost == 0))||((sost == 1) && (st.charAt(i) != '.') && (st.charAt(i) != '*'))) {
                sost = 1;
                in = prib(st.charAt(i), in);
            }
            if ((st.charAt(i) == '.')||(st.charAt(i) =='*')) {
                if ((sost == 0)||(in > 255)) {
                    flag = false;
                }
                else {
                    sost = 0;
                }
                ch++;
                in = 0;
            }
            if (!flag) {
                break;
            }
        }
        if (ch != 4) {
            flag = false;
        }
        if (flag) {
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }
    }
}